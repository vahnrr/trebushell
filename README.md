> Utility to build, compare and launch 90kg shell environments to targets over 300 meters away.
> Modules and configurations are made out of shell code.

> **Warning:** This is a POSIX-compliant shell script, error handling doesn't (and _won't_) cover **all** edge cases, so expect breakage if the usage guidelines aren't followed.

Dependencies:
- Busybox or Coreutils (for `cat`, `chmod`, `diff` and `mkdir`)
- OpenSSL (for `scp` and `ssh`)

# Usage

See the output of the `trebushell --help` command.

Configurations usually follow the `<user>@<host>` convention.
Although they can be stored inside sub-directories.
For instance, the file `configurations/project/user@host.sh` is built using `trebushell build project/user@host`

The locations used to find and create files are defined by the following environment variables:
- `TREBUSHELL_CONFIGURATIONS_DIR` Shell code configurations of targets
- `TREBUSHELL_ENVIRONMENTS_DIR` Build outputs
- `TREBUSHELL_MODULES_DIR` Shell code modules

When none of these are defined, either `$XDG_CONFIG_DIR/trebushell` or `$HOME/.config/trebushell` is used.

## Tasks

### `build`

Build a shell environment by:
1. Parsing a configuration
2. Writting its following properties to a file:
   - Identity (name and description)
   - SSH credentials
   - Modules' content
   - Extra shell code (_optional_ property)

### `compare`

Compare the already launched environment of the target with the last locally built version of it.

### `launch`

Launch a previously built environment to its target through SSH.

## Configuration

```shell
# Short name that fits one line.
name='Docker user'
# More or less long description of the host, can span multiple lines.
description='User that controls the SWARM through obscure commands.'

# To which user and host this configuration should be deployed to.
target_user='dock_her'
target_host='cargo-ship'
# The deployment process adds a sourcing operation to the shell's RC file, so
# it needs to know where to add this operation to.
#
# NOTE: Optional, defaults to 'bash'.
target_shell='zsh'

# Modules to include, the path to the modules directory (prefix) and the '.sh'
# extension (suffix) will be added when building the configuration, so stick to
# these short path to modules.
modules='
alias/any
fix/bash
tool/docker
'

# Extra shell code that's specific to this target, can span multiple lines, will
# be added after all the modules' content (so that it may override them).
#
# NOTE: Optional.
extra='
alias TODO="su svs"
# NOTE: This comment will be added to the built environment.
alias TODO="echo hey"
'
```
