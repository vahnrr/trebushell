#
# Only ever have one SSH agent running.
#

# Location of the SSH agent's environment.
export SSH_AGENT_ENV="${HOME}/.ssh/agent_env"

start_ssh_agent() {
  # When loading the agent's environment, discard its instruction that prints
  # the PID.
  ssh-agent | sed 's/^echo.*//' >"${SSH_AGENT_ENV}"

  # Make the SSH agent's environment file only accessible to the right user.
  chmod 700 "${SSH_AGENT_ENV}"

  # Load the agent's environment into the shell.
  . "${SSH_AGENT_ENV}" >/dev/null

  # Load every SSH key, which means no need to type in any password ever.
  ssh-add
}

# Load the SSH agent's environment if it's already running.
if [ -f "${SSH_AGENT_ENV}" ]
then
  # Load the agent's environment into the shell.
  . "${SSH_AGENT_ENV}" >/dev/null

  # If no SSH agent environment was loaded. Or if an SSH agent environment was
  # loaded but it doesn't seem to be pointing to a running agent. Then start a
  # new SSH agent.
  if [ -z "${SSH_AGENT_PID}" ] \
    || ( [ -n "${SSH_AGENT_PID}" ] && ! pgrep -s "${SSH_AGENT_PID}" >/dev/null )
  then
    start_ssh_agent
  fi

# Otherwise if no SSH agent is running, then start it.
else
  start_ssh_agent
fi

# Removed as to not pollute the shell after its initialisation.
unset start_ssh_agent
