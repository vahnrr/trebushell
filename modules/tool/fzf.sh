if command -v fzf >/dev/null
then
  # Make fzf's layout look like the prompt.
  export FZF_DEFAULT_OPTS="--height=8 --layout=reverse --prompt='» ' --pointer='>' --color=bg+:-1,fg:black,fg+:regular:white,header:bold:white,hl:blue,hl+:bold:blue,info:blue,pointer:blue,prompt:blue,query:regular:white,separator:black,spinner:white"

  if [ "${SHELL##*/}" = 'bash' ]
  then
    # Leverage fzf's interactivity in interactive shell sessions.
    # - Alt+C:  Go to the selected directory
    # - Ctrl+R: Paste a command from the history into the command line
    # - Ctrl+T: Paste a file's path into the command line
    #
    # NOTE: Some rely on GNU's version of 'find'.
    . '/usr/share/bash/plugins/fzf/key-bindings.bash'
  fi

  # Alt+f Append a piped call to 'fzf' at the end of the prompt.
  bind '"\ef": "\C-e | fzf"'
fi
