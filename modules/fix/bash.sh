# Don't enable these for shells opened through 'ssh' and the likes (known as
# 'dumb') or shells that aren't interactive.
#
# This module is made for bash, so there's no point enforcing POSIX compliance.
# shellcheck disable=3010,3028
if [ "${TERM}" != 'dumb' ] && [[ "${SHELLOPTS}" =~ (vi|emacs) ]]
then
  # Case-insensitive completion when TAB-ing.
  bind -s 'set completion-ignore-case on'

  # Ctrl+Delete: Delete the word after the cursor.
  bind '"5~": kill-word'
fi
