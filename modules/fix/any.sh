# Most default 'LS_COLORS' mess with the terminal's colours, so disable them.
unset LS_COLORS

# Prevent Tramp from hanging on its regex parsing of the prompt.
[ "${TERM}" = 'dumb' ] && PS1='$ '
