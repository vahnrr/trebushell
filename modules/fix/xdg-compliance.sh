# Keep things slightly organised using the XDG convention.
export XDG_CACHE_HOME="${HOME}/.cache"
export XDG_CONFIG_HOME="${HOME}/.config"
export XDG_DATA_HOME="${HOME}/.local/share"

#
# Force XDG compliance for some programmes.
#
# NOTE: https://wiki.archlinux.org/title/XDG_Base_Directory#Supported
command -v docker >/dev/null && export DOCKER_CONFIG="${XDG_CONFIG_HOME}/docker"
command -v sqlite3 >/dev/null \
  && export SQLITE_HISTORY="${XDG_CACHE_HOME}/sqlite_history"
# It's supposed to evaluate when the shell environment is loading, so it's fine.
# shellcheck disable=SC2139
command -v wget >/dev/null \
  && alias wget="wget --hsts-file='${XDG_CACHE_HOME}/wget_history'"

if command -v aws >/dev/null
then
  export AWS_CONFIG_FILE="${XDG_CONFIG_HOME}/aws/config"
  export AWS_SHARED_CREDENTIALS_FILE="${XDG_CONFIG_HOME}/aws/credentials"
fi
