base_directory="${HOME}/code/workshop/scripts"
override_directory="${base_directory}/override"
sourced_directory="${base_directory}/sourced"

if [ -d "${base_directory}" ]
then
  # Load regular script into the 'PATH'.
  export PATH="${PATH}:${base_directory}"

  # Scripts that override existing namespaces (such as wrappers) should have the
  # priority inside the 'PATH'
  [ -d "${override_directory}" ] \
    && export PATH="${override_directory}:${PATH}"

  # Scripts that alter the working directory need to be sourced to affect the
  # shell. An alias that sources them adds them to the 'PATH'.
  if [ -d "${sourced_directory}" ]
  then
    for script in "${sourced_directory}"/*
    do
      # Only load executable scripts.
      #
      # It's supposed to expand when it runs.
      # shellcheck disable=2139
      #
      # Aliases are defined this way.
      # shellcheck disable=2140
      [ -x "${script}" ] && alias "${script##*/}"=". '${script}'"
    done
  fi
fi

# Removed as to not pollute the shell after its initialisation.
unset base_directory override_directory sourced_directory
