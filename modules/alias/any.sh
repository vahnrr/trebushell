alias k9='kill -9' # Vanquish the memory hoarders (with the dogs unit).
alias ll='ls -l'   # Detailed list of files.
alias s='sudo'     # Grant superuser rights for a command.

# $1: Directory to get into and list the content of.
cdls() {
  cd "${1}" && ls -l
}

# $1: Directory to create and then get into.
mkcd() {
  # 'mkdir' creates that directory, so it's expected to exist.
  # shellcheck disable=2164
  mkdir -p "${1}" && cd "${1}"
}

#
# Fallback aliases.
#

# Ripgrep equivalent (doesn't follow symlinks).
command -v rg >/dev/null \
  || alias rg='grep --line-number --recursive --colour=always'
