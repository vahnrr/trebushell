# $1: Intended command when a typo is entered.
# $*: Typos to interpret as the intended command.
set_typo() {
  intended="${1}"
  shift

  # Only set these aliases if the intended command is in the '$PATH'.
  if command -v "${intended}" >/dev/null
  then
    # It has to expand for the loop to take place, so it's intended.
    # shellcheck disable=2068
    for typo in $@
    do
      # Should be defined when evaluated, not when used interactively.
      # shellcheck disable=2139
      alias "${typo}"="${intended}"
    done
  fi
}

set_typo 'docker' 'dockre' 'dcoker' 'dcokre'
set_typo 'git'    'gti'

# Removed as to not pollute the shell after its initialisation.
unset set_typo
