name='Dev workstation'
description="Main dev workstation hosted in the cloud.
(this description can span multiple lines)"

# SSH credentials.
target_host='work'
target_user='vahnrr'
target_shell='zsh'

modules='
alias/any
alias/typo
fix/any
fix/bash
tool/fzf
'

extra='
echo "Welcome to the dev workstation!"
. /shared/bin/project.env
'
